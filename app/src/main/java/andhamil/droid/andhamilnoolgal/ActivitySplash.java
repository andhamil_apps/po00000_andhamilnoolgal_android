/*
 * ActivitySplash
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2014
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import andhamil.libtamil.Konstant;
import andhamil.libtamil.Utils;
import andhamil.libtamil.view.DialogRate;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class ActivitySplash extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash);

		Utils.FONT_TAMIL = Typeface.createFromAsset(getAssets(), Konstant.FILENAME_FONT_BAMINI);

		try
		{
			InputStream is = getResources().openRawResource(R.raw.andhamil_noolgal);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			String strJSON = new String(buffer, "UTF-8");
			ParserJSON.decode(strJSON);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		SharedPreferences preferences = getSharedPreferences(ProjectKonstants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor prefEditor = preferences.edit();

		int nRateThisApp = preferences.getInt(Konstant.PREF_RATE_THIS_APP, Konstant.DEF_RATE_THIS_APP);
		int nLaunchCount = preferences.getInt(Konstant.PREF_LAUNCH_COUNT, Konstant.DEF_LAUNCH_COUNT);
		nLaunchCount++;
		prefEditor.putInt(Konstant.PREF_LAUNCH_COUNT, nLaunchCount);
		prefEditor.commit();

		if ((nRateThisApp == Konstant.ID_RATE_LATER) && (nLaunchCount % 7 == 0))
		{
			final DialogRate rateDialog = new DialogRate(ActivitySplash.this);
			rateDialog.displayDialog(preferences);
			rateDialog.setOnDismissListener(new OnDismissListener()
			{
				@Override
				public void onDismiss(DialogInterface dialog)
				{
					switch (rateDialog.getUserSelection())
					{
					case Konstant.ID_RATE_YEAH:
						switch (Konstant.APPSTORE_INSTALLED_FROM)
						{
						case Konstant.ID_APPSTORE_GOOGLEPLAY:
							Intent intentAppOnGooglePlay = new Intent(Intent.ACTION_VIEW);
							intentAppOnGooglePlay.setData(Uri.parse(getString(R.string.app_link_on_google_play_store)));
							startActivity(intentAppOnGooglePlay);
							finish();
							break;
						case Konstant.ID_APPSTORE_AMAZON:
							Intent intentAppOnAmazonAppstore = new Intent(Intent.ACTION_VIEW);
							intentAppOnAmazonAppstore.setData(Uri.parse(getString(R.string.app_link_on_amazon_appstore)));
							startActivity(intentAppOnAmazonAppstore);
							finish();
							break;
						}
						break;
					default:
						EnAppLaunchingTask enLauncher = new EnAppLaunchingTask();
						enLauncher.execute();
					}
				}
			});
		}
		else
		{
			EnAppLaunchingTask enLauncher = new EnAppLaunchingTask();
			enLauncher.execute();
		}
	}

	private void launchApplication()
	{
		Intent intentCheiyul = new Intent(ActivitySplash.this, ActivityHome.class);
		startActivity(intentCheiyul);
		finish();
	}

	private int validateApplication()
	{
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.CANADA);
			Date dateExpiry = sdf.parse(Konstant.APPLICATION_EXPIRY_DATE);
			Date dateCurrent = sdf.parse(sdf.format(new Date()));
			if (dateExpiry.before(dateCurrent))
				return Konstant.ID_VALIDITY_EXPIRED;
		}
		catch (ParseException e)
		{
			return Konstant.ID_VALIDITY_NOT_VALIDATED;
		}
		return Konstant.ID_VALIDITY_VALID;
	}

	public void showLicenceError(String strMessage)
	{
		AlertDialog.Builder dialogError = new AlertDialog.Builder(this);
		dialogError.setTitle(R.string.app_name_english);

		TextView tvMessage = new TextView(this);
		tvMessage.setPadding(10, 10, 10, 10);
		tvMessage.setTypeface(Utils.FONT_ENGLISH);
		tvMessage.setTextColor(Color.BLACK);
		tvMessage.setText(strMessage);
		dialogError.setView(tvMessage);

		switch (Konstant.APPSTORE_INSTALLED_FROM)
		{
		case Konstant.ID_APPSTORE_GOOGLEPLAY:
			dialogError.setPositiveButton(getString(R.string.google_play_store), new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int whichButton)
				{
					dialog.dismiss();
					finish();

					String strURL = getString(R.string.app_link_on_google_play_store);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(strURL));
					startActivity(intent);
				}
			});
			break;

		case Konstant.ID_APPSTORE_AMAZON:
			dialogError.setPositiveButton(getString(R.string.amazon_appstore), new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int whichButton)
				{
					dialog.dismiss();
					finish();

					String strURL = getString(R.string.app_link_on_amazon_appstore);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(strURL));
					startActivity(intent);
				}
			});
			break;
		}

		dialogError.show();
	}

	private class EnAppLaunchingTask extends AsyncTask<Void, Void, Void>
	{
		private int mnValidity;

		EnAppLaunchingTask()
		{
			mnValidity = Konstant.ID_VALIDITY_NOT_VALIDATED;
		}

		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			mnValidity = validateApplication();
			Utils.FONT_TAMIL = Typeface.createFromAsset(getAssets(), Konstant.FILENAME_FONT_BAMINI);
			Utils.loadInstalledApps(ActivitySplash.this);
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... params)
		{
		}

		@Override
		protected void onPostExecute(Void param)
		{
			if (mnValidity == Konstant.ID_VALIDITY_VALID)
			{
				TimerTask pTimerTask = new TimerTask()
				{
					@Override
					public void run()
					{
						launchApplication();
					}
				};
				Timer pTimer = new Timer();
				pTimer.schedule(pTimerTask, 2500);
			}
			else if (mnValidity == Konstant.ID_VALIDITY_EXPIRED)
				showLicenceError(getString(R.string.msg_app_license_expired));
			else
				showLicenceError(getString(R.string.msg_app_license_not_validated));
		}
	}
}