package andhamil.droid.andhamilnoolgal;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class FragmentSlidingMenu extends Fragment
{
	ListView mListGroups;
	AdapterSlidingMenu mListAdapter;
	FragmentManager mFragmentManager;
	ActivityHome mActivity;

	public FragmentSlidingMenu()
	{
		mFragmentManager = null;
		mActivity = null;
	}

	public void setFragmentSlidingMenu(FragmentManager fm, ActivityHome activity)
	{
		mFragmentManager = fm;
		mActivity = activity;
	}

	@SuppressLint("InflateParams")
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_slidingmenu, null);
		setUpView(root);
		return root;
	}

	void setUpView(ViewGroup root)
	{
		mListGroups = (ListView) root.findViewById(R.id.lv_slidingmenu);
		initList();
		setUpClick();
	}

	void initList()
	{
		mListAdapter = new AdapterSlidingMenu(getActivity(), ProjectUtils.getGroupNames());
		mListGroups.setAdapter(mListAdapter);
	}

	void setUpClick()
	{
		mListGroups.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				mActivity.closeDrawer();
				mFragmentManager.beginTransaction().replace(R.id.main_frame, FragmentHome.newInstance(position, mActivity)).commit();
			}
		});
	}
}
