package andhamil.droid.andhamilnoolgal;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Utils;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class FragmentHome extends Fragment
{
	private static final String ARG_SECTION_NUMBER = "section_number";

	private AdapterBooks mAdapterBooks;

	private int mGroupIndex;
	private Context mContext;

    public FragmentHome()
    {
    }

	public static FragmentHome newInstance(int sectionNumber, Context context)
	{
		FragmentHome fragment = new FragmentHome();
        fragment.setFragmentHome(sectionNumber, context);
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public void setFragmentHome(int sectionNumber, Context context)
	{
		mAdapterBooks = null;
		mGroupIndex = sectionNumber;
		mContext = context;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_home, container, false);

		if (mGroupIndex < 1)
		{
			TextView tvGroupName = (TextView) rootView.findViewById(R.id.tv_groupname);
			tvGroupName.setTypeface(Utils.FONT_TAMIL);
			tvGroupName.setText(EncoderFontBamini.encode(ProjectKonstants.ALL_BOOKS));

			mAdapterBooks = new AdapterBooks(mContext, R.layout.listitem_book, ProjectUtils.getAllBookNames(), ProjectUtils.getAllPackageNames());
		}
		else
		{
			TextView tvGroupName = (TextView) rootView.findViewById(R.id.tv_groupname);
			tvGroupName.setTypeface(Utils.FONT_TAMIL);
			tvGroupName.setText(EncoderFontBamini.encode(ProjectUtils.getGroupNames().get(mGroupIndex)));

			mAdapterBooks = new AdapterBooks(mContext, R.layout.listitem_book, ProjectUtils.getBookNames(mGroupIndex - 1), ProjectUtils.getPackageNames(mGroupIndex - 1));
		}

		ListView listView = (ListView) rootView.findViewById(R.id.lv_list_books);
		listView.setAdapter(mAdapterBooks);
		listView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int nPosition, long id)
			{

			}
		});
		return rootView;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}

	@Override
	public void onResume()
	{
		super.onResume();

		Utils.loadInstalledApps(mContext);
		mAdapterBooks.notifyDataSetChanged();
	}
}
