package andhamil.droid.andhamilnoolgal;

import java.io.IOException;
import java.util.ArrayList;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.content.Context;
import android.os.AsyncTask;

public class ProjectUtils
{

	
	public static ArrayList<String> getGroupNames()
	{
		ArrayList<String> alsGroupNames = new ArrayList<String>();
		alsGroupNames.add(ProjectKonstants.ALL_BOOKS);

		for (int i = 0; i < ProjectKonstants.BOOK_DATA.getGroups().size(); i++)
		{
			alsGroupNames.add(ProjectKonstants.BOOK_DATA.getGroups().get(i).getGroupName());
		}
		
		return alsGroupNames;
	}

	public static ArrayList<String> getBookNames(int nGroupIndex)
	{
		ArrayList<String> alsBookNames = new ArrayList<String>();

		if (nGroupIndex < ProjectKonstants.BOOK_DATA.getGroups().size())
		{
			for (int i = 0; i < ProjectKonstants.BOOK_DATA.getGroups().get(nGroupIndex).getBooks().size(); i++)
			{
				alsBookNames.add(ProjectKonstants.BOOK_DATA.getGroups().get(nGroupIndex).getBooks().get(i).getBookName());
			}
		}
		return alsBookNames;
	}

	public static ArrayList<String> getAllBookNames()
	{
		ArrayList<String> alsBookNames = new ArrayList<String>();

		for (int i = 0; i < ProjectKonstants.BOOK_DATA.getGroups().size(); i++)
		{
			Group group = ProjectKonstants.BOOK_DATA.getGroups().get(i);
			for (int j = 0; j < group.getBooks().size(); j++)
			{
				Book book = group.getBooks().get(j);
				alsBookNames.add(book.getBookName());
			}
		}
		return alsBookNames;
	}

	public static ArrayList<String> getPackageNames(int nGroupIndex)
	{
		ArrayList<String> alsBookNames = new ArrayList<String>();

		if (nGroupIndex < ProjectKonstants.BOOK_DATA.getGroups().size())
		{
			for (int i = 0; i < ProjectKonstants.BOOK_DATA.getGroups().get(nGroupIndex).getBooks().size(); i++)
			{
				alsBookNames.add(ProjectKonstants.BOOK_DATA.getGroups().get(nGroupIndex).getBooks().get(i).getPackageName());
			}
		}
		return alsBookNames;
	}

	public static ArrayList<String> getAllPackageNames()
	{
		ArrayList<String> alsBookNames = new ArrayList<String>();

		for (int i = 0; i < ProjectKonstants.BOOK_DATA.getGroups().size(); i++)
		{
			Group group = ProjectKonstants.BOOK_DATA.getGroups().get(i);
			for (int j = 0; j < group.getBooks().size(); j++)
			{
				Book book = group.getBooks().get(j);
				alsBookNames.add(book.getPackageName());
			}
		}
		return alsBookNames;
	}

	public static String constructRecommendMsgTamil(Context context)
	{
		String strReturn = context.getString(R.string.recommend_body);
		strReturn += context.getString(R.string.recommend_body_google_play_store);
		strReturn += context.getString(R.string.app_link_on_google_play_store);
		strReturn += context.getString(R.string.recommend_body_amazon_app_store);
		strReturn += context.getString(R.string.app_link_on_amazon_appstore);

		return strReturn;
	}
}
