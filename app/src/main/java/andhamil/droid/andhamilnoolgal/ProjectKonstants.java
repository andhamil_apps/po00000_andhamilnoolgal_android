/*
 * ProjectKonstants
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2014
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

public interface ProjectKonstants
{
	// Application

	public static final String APPLICATION_SKU_FOR_DONATION = "donation_to_andhamilnoolgal";
	public static final String APP_PUBLIC_KEY_GOOGLE_PLAY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkZrWx+91/0vNhTqz/+5UOAX8sYMOLEQGsBRik60MbPDmqxtCQ11z2aD9aIEqyjMpZa5CbN05JXLH6WQhRsCpyuOO01vMclyHCcB2LtT5jK51EplFUKGhDgCvN2ZIZhG4EKckQeNzZ9fv7c4YVR12FbwJJ3QP7zMtCiXUOOUYWYXwFCJkLNF9qCMIVsFvg2IRFKc66apCskD+XFMNi8y8SIgsQ5DzOlQuYK5V/9f2K7PXE0MfsDcH3XwZTHReS2jN4NpFKvvQJU0KXDy7zAIe0GflbX6RoAvXeNJTM+doitQrFQn2/ocloFYu6UZxyn/5EiIRzFrGkfHfUcogHR/LoQIDAQAB";

	// Shared Preferences

	public static final String PREFERENCES_FILE_NAME = "andhamil_droid_andhamilnoolgal_preferences";

	// Data

	public DataStorage BOOK_DATA = new DataStorage();

	// Utility Strings
	public String ALL_BOOKS = "அனைத்து நூல்கள்";
}
