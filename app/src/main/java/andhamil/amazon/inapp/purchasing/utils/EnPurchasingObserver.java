/*
 * EnPurchasingObserver
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2014
 * Confidential and proprietary.
 */

package andhamil.amazon.inapp.purchasing.utils;

import andhamil.droid.andhamilnoolgal.R;
import andhamil.libtamil.Utils;
import android.content.Context;

import com.amazon.inapp.purchasing.BasePurchasingObserver;
import com.amazon.inapp.purchasing.ItemDataResponse;
import com.amazon.inapp.purchasing.PurchaseResponse;

public class EnPurchasingObserver extends BasePurchasingObserver
{
	private Context mContext;

	public EnPurchasingObserver(Context context)
	{
		super(context);
		mContext = context;
	}

	@Override
	public void onItemDataResponse(ItemDataResponse itemDataResponse)
	{
	}

	@Override
	public void onPurchaseResponse(PurchaseResponse purchaseResponse)
	{
		switch (purchaseResponse.getPurchaseRequestStatus())
		{
		case SUCCESSFUL:
				Utils.showToastMsgInTamil(mContext, mContext.getString(R.string.msg_confirmation_purchase_tamil));
		case ALREADY_ENTITLED:
			break;
		case FAILED:
			break;
		case INVALID_SKU:
			break;
		default:
			break;
		}
	}
}