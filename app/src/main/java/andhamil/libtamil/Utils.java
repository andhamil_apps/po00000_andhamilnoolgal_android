/*
 * Utils
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2015.
 * Confidential and proprietary.
 */

package andhamil.libtamil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import andhamil.droid.andhamilnoolgal.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Utils
{
	public static Typeface FONT_TAMIL = null;
	public static Typeface FONT_ENGLISH = Typeface.SERIF;

	public static String captureScreen(View view, String strCompanayName, String strAppName) throws IOException
	{
		String strExtStorage = Environment.getExternalStorageDirectory().toString();
		strExtStorage += File.separator + strCompanayName;
		File fileDirectory = new File(strExtStorage);
		if (!fileDirectory.exists())
		{
			fileDirectory.mkdir();
		}
		strExtStorage += File.separator + strAppName;
		fileDirectory = new File(strExtStorage);
		if (!fileDirectory.exists())
		{
			fileDirectory.mkdir();
		}
		String strFileName = getCurrentTimeStamp() + ".png";
		File fileImage = new File(fileDirectory.getAbsolutePath(), strFileName);

		try
		{
			if (view != null)
			{
				Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				view.draw(canvas);
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.PNG, 40, bytes);
				fileImage.createNewFile();
				FileOutputStream fo = new FileOutputStream(fileImage);
				fo.write(bytes.toByteArray());
				fo.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return fileImage.getAbsolutePath();
	}

	public static String constructHTMLDataTamil(String strData, int nFontSize)
	{
		String strReturn = "";
		strReturn += "<HTML>";
		strReturn += "<HEAD>";
		strReturn += "<STYLE TYPE=\"text/css\">";
		strReturn += "@font-face{font-family:'enFont';src:url(\"file:///android_asset/" + Konstant.FILENAME_FONT_BAMINI + "\");}";
		strReturn += "body{font-family:'enFont';font-size:" + nFontSize + "px;text-align:left;}";
		strReturn += "</STYLE>";
		strReturn += "</HEAD>";
		strReturn += "<BODY>";
		strReturn += EncoderFontBamini.encode(strData.replace("\n", "<BR>"));
		strReturn += "</BODY>";
		strReturn += "</HTML>";
		return strReturn;
	}

	public static String getCurrentTimeStamp()
	{
		String strReturn = "";

		Calendar calendar = Calendar.getInstance();
		strReturn = Integer.toString(calendar.get(Calendar.YEAR));
		strReturn += Integer.toString(calendar.get(Calendar.MONTH));
		strReturn += Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
		strReturn += "_" + Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
		strReturn += Integer.toString(calendar.get(Calendar.MINUTE));
		strReturn += Integer.toString(calendar.get(Calendar.SECOND));

		return strReturn;
	}

	public static void loadInstalledApps(Context context)
	{
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		final List<ResolveInfo> listInstalledApps = context.getPackageManager().queryIntentActivities(mainIntent, 0);

		Konstant.INSTALLED_APPS.clear();
		for (ResolveInfo appInfo : listInstalledApps)
		{
			Konstant.INSTALLED_APPS.add(appInfo.activityInfo.packageName);
		}
	}

	public static void shareImage(Activity activity, Context context, String strAppNameWithoutSpace, String strAppNameInTamil, String strAppNameInEnglish)
	{
		try
		{
			String strScreenshot = captureScreen(activity.getWindow().getDecorView().getRootView(), context.getString(R.string.company_name_english), strAppNameWithoutSpace);
			Uri uri = Uri.fromFile(new File(strScreenshot));

			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType("image/jpeg");
			shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

			if (android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
				context.startActivity(Intent.createChooser(shareIntent, strAppNameInEnglish));
			else
				context.startActivity(Intent.createChooser(shareIntent, strAppNameInTamil));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void shareText(Context context, String strSubject, String strContent, String strAppNameInTamil, String strAppNameInEnglish)
	{
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, strSubject);
		shareIntent.putExtra(Intent.EXTRA_TEXT, strContent);

		if (android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
			context.startActivity(Intent.createChooser(shareIntent, strAppNameInEnglish));
		else
			context.startActivity(Intent.createChooser(shareIntent, strAppNameInTamil));
		return;
	}

	public static void showToastMsgInTamil(Context context, String strMessage)
	{
		Toast toast = Toast.makeText(context, EncoderFontBamini.encode(strMessage), Toast.LENGTH_LONG);
		TextView tvMessage = (TextView) toast.getView().findViewById(android.R.id.message);
		tvMessage.setTypeface(FONT_TAMIL);
		toast.show();
	}
}