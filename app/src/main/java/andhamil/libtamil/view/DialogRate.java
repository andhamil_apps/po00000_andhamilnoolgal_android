/*
 * DialogRate
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2015.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.droid.andhamilnoolgal.R;
import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Konstant;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class DialogRate extends Dialog
{
	private Context mContext;

	private int mnUserSelection;

	public DialogRate(Context context)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_rate);
		this.setCancelable(false);

		mContext = context;
		

		if (Utils.FONT_TAMIL == null)
		{
			Utils.FONT_TAMIL = Typeface.createFromAsset(context.getAssets(), Konstant.FILENAME_FONT_BAMINI);
		}
	}

	public void displayDialog(SharedPreferences preferences)
	{
		final SharedPreferences.Editor prefEditor = preferences.edit();
		
		TextView tvTitle = (TextView) findViewById(R.id.tv_rate_title);
		tvTitle.setTypeface(Utils.FONT_TAMIL);
		tvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.rate_this_app)));

		TextView tvRatePrompt = (TextView) findViewById(R.id.tv_rate_text);
		tvRatePrompt.setTypeface(Utils.FONT_TAMIL);
		tvRatePrompt.setText(EncoderFontBamini.encode(mContext.getString(R.string.rate_this_app_prompt)));

		TextView tvYeah = (TextView) findViewById(R.id.tv_rate_yeah);
		tvYeah.setTypeface(Utils.FONT_TAMIL);
		tvYeah.setText(EncoderFontBamini.encode(mContext.getString(R.string.yeah)));
		tvYeah.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mnUserSelection = Konstant.ID_RATE_YEAH;
				
				prefEditor.putInt(Konstant.PREF_RATE_THIS_APP, Konstant.ID_RATE_YEAH);
				prefEditor.commit();

				dismiss();
			}
		});

		TextView tvNope = (TextView) findViewById(R.id.tv_rate_nope);
		tvNope.setTypeface(Utils.FONT_TAMIL);
		tvNope.setText(EncoderFontBamini.encode(mContext.getString(R.string.nope)));
		tvNope.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mnUserSelection = Konstant.ID_RATE_NOPE;
				
				prefEditor.putInt(Konstant.PREF_RATE_THIS_APP, Konstant.ID_RATE_NOPE);
				prefEditor.commit();

				dismiss();
			}
		});

		TextView tvLater = (TextView) findViewById(R.id.tv_rate_later);
		tvLater.setTypeface(Utils.FONT_TAMIL);
		tvLater.setText(EncoderFontBamini.encode(mContext.getString(R.string.later)));
		tvLater.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mnUserSelection = Konstant.ID_RATE_LATER;
				
				prefEditor.putInt(Konstant.PREF_RATE_THIS_APP, Konstant.ID_RATE_LATER);
				prefEditor.commit();

				dismiss();
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}
	
	public int getUserSelection()
	{
		return mnUserSelection;
	}
	
}