/*
 * EnTextView
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2015.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.droid.andhamilnoolgal.R;
import andhamil.libtamil.Konstant;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

public class EnTextView extends TextView
{
	private static final String TAG = "TextView";

	public EnTextView(Context context)
	{
		super(context);
	}

	public EnTextView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setCustomFont(context, attrs);
	}

	public EnTextView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setCustomFont(context, attrs);
	}

	private void setCustomFont(Context ctx, AttributeSet attrs)
	{
		TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.EnTextView);
		String customFont = a.getString(R.styleable.EnTextView_customFont);
		setCustomFont(ctx, customFont);
		a.recycle();
	}

	public boolean setCustomFont(Context ctx, String asset)
	{
		Typeface tf = null;
		try
		{
			tf = Typeface.createFromAsset(ctx.getAssets(), Konstant.FILENAME_FONT_BAMINI);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Could not get typeface: " + e.getMessage());
			return false;
		}

		setTypeface(tf);
		return true;
	}
}