/*
 * DialogAbout
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2015.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.droid.andhamilnoolgal.R;
import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Xml.Encoding;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

public class DialogAbout extends Dialog
{
	private Context mContext;

	public DialogAbout(Context context)
	{
		super(context);
		mContext = context;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_about);
	}

	public void displayDialog(String strIntro, String strAck)
	{
		TextView tvTitle = (TextView) findViewById(R.id.tv_about_title);
		tvTitle.setTypeface(Utils.FONT_TAMIL);
		tvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.about)));

		WebView wvAck = (WebView) findViewById(R.id.wv_about_acknowledgement);
		String strAckHTML = Utils.constructHTMLDataTamil(strAck, mContext.getResources().getInteger(R.integer.font_m));
		wvAck.loadDataWithBaseURL("file:///android_asset/", strAckHTML, "text/html", Encoding.UTF_8.toString(), null);
		wvAck.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				return true;
			}
		});

		WebView wvSocialConnection = (WebView) findViewById(R.id.wv_about_social_connection);
		String strSocialConnection = Utils.constructHTMLDataTamil(mContext.getString(R.string.social_network_prompt), mContext.getResources().getInteger(R.integer.font_m));
		wvSocialConnection.loadDataWithBaseURL("file:///android_asset/", strSocialConnection, "text/html", Encoding.UTF_8.toString(), null);
		wvSocialConnection.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				return true;
			}
		});

		ImageButton ibFacebook = (ImageButton) findViewById(R.id.ib_about_facebook);
		ibFacebook.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnFbURL = mContext.getString(R.string.page_link_on_facebook);
				Intent intentPageOnFb = new Intent(Intent.ACTION_VIEW);
				intentPageOnFb.setData(Uri.parse(strPageOnFbURL));
				mContext.startActivity(intentPageOnFb);
			}
		});

		ImageButton ibTwitters = (ImageButton) findViewById(R.id.ib_about_twitter);
		ibTwitters.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnTwitterURL = mContext.getString(R.string.page_link_on_twitter);
				Intent intentPageOnTwitter = new Intent(Intent.ACTION_VIEW);
				intentPageOnTwitter.setData(Uri.parse(strPageOnTwitterURL));
				mContext.startActivity(intentPageOnTwitter);
			}
		});

		ImageButton ibGooglePlus = (ImageButton) findViewById(R.id.ib_about_googleplus);
		ibGooglePlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnGpURL = mContext.getString(R.string.page_link_on_googleplus);
				Intent intentPageOnGp = new Intent(Intent.ACTION_VIEW);
				intentPageOnGp.setData(Uri.parse(strPageOnGpURL));
				mContext.startActivity(intentPageOnGp);
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.FLAG_FULLSCREEN;
		getWindow().setAttributes(lp);
		show();
	}
}