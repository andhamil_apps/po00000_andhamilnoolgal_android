/*
 * Konstant
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2015.
 * Confidential and proprietary.
 */

package andhamil.libtamil;

import java.util.ArrayList;

public interface Konstant
{
	// Filenames

	public static String FILENAME_FONT_BAMINI = "fonts/tamil_bamini.ttf";

	// Application Versions

	public static final String ANDROID_ICS = "4.0";
	public static final String ANDROID_JB_1 = "4.1";
	public static final String ANDROID_JB_2 = "4.2";
	public static final String ANDROID_KK_1 = "4.3";
	public static final String ANDROID_KK_2 = "4.4";
	public static final String ANDROID_LP = "5.0";

	// Application

	public static final String APPLICATION_EXPIRY_DATE = "2016/07/15";

	public static int ID_VALIDITY_VALID = 0;
	public static int ID_VALIDITY_EXPIRED = 1;
	public static int ID_VALIDITY_NOT_VALIDATED = 2;

	public static int ID_RATE_YEAH = 0;
	public static int ID_RATE_NOPE = 1;
	public static int ID_RATE_LATER = 2;

	public static int ID_APPSTORE_GOOGLEPLAY = 0;
	public static int ID_APPSTORE_AMAZON = 1;

	public static int ID_SHARE_IMAGE = 0;
	public static int ID_SHARE_TEXT = 1;

	// Constants

	public static int APPSTORE_INSTALLED_FROM = ID_APPSTORE_GOOGLEPLAY;

	// Shared Preferences

	public static final String PREF_RATE_THIS_APP = "RATE_THIS_APP";
	public static int DEF_RATE_THIS_APP = ID_RATE_LATER;

	public static final String PREF_LAUNCH_COUNT = "LAUNCH_COUNT_FOR_RATING";
	public static int DEF_LAUNCH_COUNT = 0;

	public static final String PREF_CURRENT_CHEIYUL_NO = "CURRENT_CHEIYUL_NO";
	public static final int DEF_CURRENT_CHEIYUL_NO = 0;

	public static final String PREF_WIDGET_CHEIYUL_NO = "WIDGET_CHEIYUL_NO";
	public static final int DEF_WIDGET_CHEIYUL_NO = 0;
	
	public static final String PREF_CURRENT_SCROLL_YPOS = "CURRENT_SCROLL_YPOS";
	public static final int DEF_CURRENT_SCROLL_YPOS = 0;
	
	public static final String PREF_APP_BRIGHTNESS = "APP_BRIGHTNESS";
	public static int DEF_APP_BRIGHTNESS = 0;

	public static final String PREF_FAV_CHEIYUL = "FAV_CHEIYUL";

	// Exchange Keys

	public static String KEY_CHEIYUL_NO = "KEY_CHEIYUL_NO";
	
	public static final String ARG_PAGE = "page";

	// Data

	public ArrayList<String> INSTALLED_APPS = new ArrayList<String>();
	public String ANDHAMIL_BOOKS_PACKAGENAME = "andhamil.droid.andhamilnoolgal";
}