/**
 * Automatically generated file. DO NOT MODIFY
 */
package andhamil.droid.andhamilnoolgal;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "andhamil.droid.andhamilnoolgal";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 12;
  public static final String VERSION_NAME = "15.12.21";
}
