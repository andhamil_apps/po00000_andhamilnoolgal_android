/*
 * ActivitySplash
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import andhamil.libtamil.Konstant;
import andhamil.libtamil.Utils;
import andhamil.libtamil.view.DialogRate;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class ActivitySplash extends Activity
{
	// Android
	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash);

		mPreferences = getSharedPreferences(ProjectKonstants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		Utils.loadInstalledApps(ActivitySplash.this);
		Utils.loadFontFiles(getApplicationContext(), mPreferences);

		try
		{
			InputStream is = getResources().openRawResource(R.raw.andhamil_noolgal);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			String strJSON = new String(buffer, "UTF-8");
			ParserJSON.decode(strJSON);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		int nRateThisApp = mPreferences.getInt(Konstant.PREF_RATE_THIS_APP, Konstant.DEF_RATE_THIS_APP);
		int nLaunchCount = mPreferences.getInt(Konstant.PREF_LAUNCH_COUNT, Konstant.DEF_LAUNCH_COUNT);
		nLaunchCount++;
		mPrefEditor.putInt(Konstant.PREF_LAUNCH_COUNT, nLaunchCount);
		mPrefEditor.commit();

		if ((nRateThisApp == Konstant.ID_RATE_LATER) && (nLaunchCount % 7 == 0))
		{
			final DialogRate rateDialog = new DialogRate(ActivitySplash.this, mPreferences);
			rateDialog.displayDialog();
			rateDialog.setOnDismissListener(new OnDismissListener()
			{
				@Override
				public void onDismiss(DialogInterface dialog)
				{
					switch (rateDialog.getUserSelection())
					{
					case Konstant.ID_RATE_YEAH:
						Intent intentAppOnGooglePlay = new Intent(Intent.ACTION_VIEW);
						intentAppOnGooglePlay.setData(Uri.parse(getString(R.string.app_link_on_google_play_store)));
						startActivity(intentAppOnGooglePlay);
						finish();
						break;
					default:
						EnAppLaunchingTask enLauncher = new EnAppLaunchingTask();
						enLauncher.execute();
					}
				}
			});
		}
		else
		{
			EnAppLaunchingTask enLauncher = new EnAppLaunchingTask();
			enLauncher.execute();
		}
	}

	private void launchApplication()
	{
		Intent intentCheiyul = new Intent(ActivitySplash.this, ActivityHome.class);
		startActivity(intentCheiyul);
		finish();
	}

	public void showLicenceError(String strMessage)
	{
		AlertDialog.Builder dialogError = new AlertDialog.Builder(this);
		dialogError.setTitle(R.string.app_name_english);

		TextView tvMessage = new TextView(this);
		tvMessage.setPadding(10, 10, 10, 10);
		tvMessage.setTypeface(Utils.FONT_ENGLISH);
		tvMessage.setTextColor(Color.BLACK);
		tvMessage.setText(strMessage);

		dialogError.setView(tvMessage);
		dialogError.setPositiveButton(getString(R.string.google_play_store), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton)
			{
				dialog.dismiss();
				finish();

				String strURL = getString(R.string.app_link_on_google_play_store);
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(strURL));
				startActivity(intent);
			}
		});
		dialogError.show();
	}

	private class EnAppLaunchingTask extends AsyncTask<Void, Void, Void>
	{
		EnAppLaunchingTask()
		{
		}

		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			Utils.loadFontFiles(getApplicationContext(), mPreferences);

			return null;
		}

		@Override
		protected void onProgressUpdate(Void... params)
		{
		}

		@Override
		protected void onPostExecute(Void param)
		{
			TimerTask pTimerTask = new TimerTask()
			{
				@Override
				public void run()
				{
					launchApplication();
				}
			};
			Timer pTimer = new Timer();
			pTimer.schedule(pTimerTask, 2500);
		}
	}
}