/*
 * AdapterBooks
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

import java.util.ArrayList;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Konstant;
import andhamil.libtamil.Utils;
import andhamil.droid.andhamilnoolgal.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterBooks extends ArrayAdapter<String>
{
	private ArrayList<String> malsBookNames;
	private ArrayList<String> malsPackageNames;

	private Context mContext;
	private BookListItem mListItemHolder;
	private int mnLayoutResourceId;

	static class BookListItem
	{
		ImageView ivToBeDownloaded;
		ImageView ivInstalled;
		TextView tvBookName;
		TextView tvBookNumber;
	}

	public AdapterBooks(Context context, int layoutResourceId, ArrayList<String> alsBookNames, ArrayList<String> alsPackageNames)
	{
		super(context, layoutResourceId, alsBookNames);

		mContext = context;
		mnLayoutResourceId = layoutResourceId;

		malsBookNames = alsBookNames;
		malsPackageNames = alsPackageNames;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mnLayoutResourceId, parent, false);

			mListItemHolder = new BookListItem();
			mListItemHolder.tvBookNumber = (TextView) row.findViewById(R.id.tv_elc_book_number);
			mListItemHolder.tvBookName = (TextView) row.findViewById(R.id.tv_elc_book_name);
			mListItemHolder.ivToBeDownloaded = (ImageView) row.findViewById(R.id.iv_elc_to_be_downloaded);
			mListItemHolder.ivInstalled = (ImageView) row.findViewById(R.id.iv_elc_installed);

			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (BookListItem) row.getTag();
		}

		mListItemHolder.ivToBeDownloaded.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
			}
		});

		mListItemHolder.ivInstalled.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
			}
		});

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (Konstant.INSTALLED_APPS.contains(malsPackageNames.get(position)))
				{
					Intent launchIntent = mContext.getPackageManager().getLaunchIntentForPackage(malsPackageNames.get(position));
					if (launchIntent != null)
						mContext.startActivity(launchIntent);
					else
						gotoAppstoreForDownload(position);
				}
				else
				{
					gotoAppstoreForDownload(position);
				}
			}
		});

		mListItemHolder.ivInstalled.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent launchIntent = mContext.getPackageManager().getLaunchIntentForPackage(malsPackageNames.get(position));
				if (launchIntent != null)
					mContext.startActivity(launchIntent);
				else
					gotoAppstoreForDownload(position);
			}
		});

		mListItemHolder.ivToBeDownloaded.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				gotoAppstoreForDownload(position);
			}
		});

		String strFormat = "%01d";
		if (malsBookNames.size() > 0 && malsBookNames.size() < 10)
			strFormat = "%01d";
		else if (malsBookNames.size() > 9 && malsBookNames.size() < 100)
			strFormat = "%02d";
		else if (malsBookNames.size() > 99 && malsBookNames.size() < 1000)
			strFormat = "%03d";
		mListItemHolder.tvBookNumber.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mListItemHolder.tvBookNumber.setText(String.format(strFormat, (position + 1)) + ".");

		String strBookName = malsBookNames.get(position);
		mListItemHolder.tvBookName.setTypeface(Utils.FONT_TAMIL_NORMAL);
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			mListItemHolder.tvBookName.setText(EncoderFontBamini.encode(strBookName));
		else
			mListItemHolder.tvBookName.setText(strBookName);

		if (Konstant.INSTALLED_APPS.contains(malsPackageNames.get(position)))
		{
			mListItemHolder.ivInstalled.setVisibility(View.VISIBLE);
			mListItemHolder.ivToBeDownloaded.setVisibility(View.GONE);
		}
		else
		{
			mListItemHolder.ivInstalled.setVisibility(View.GONE);
			mListItemHolder.ivToBeDownloaded.setVisibility(View.VISIBLE);
		}

		return row;
	}

	private void gotoAppstoreForDownload(int nPosition)
	{
		String strAppOnGooglePlayURL = mContext.getString(R.string.generic_app_link_on_google_play_store) + malsPackageNames.get(nPosition);
		Intent intentAppOnGooglePlay = new Intent(Intent.ACTION_VIEW);
		intentAppOnGooglePlay.setData(Uri.parse(strAppOnGooglePlayURL));
		mContext.startActivity(intentAppOnGooglePlay);
	}
}