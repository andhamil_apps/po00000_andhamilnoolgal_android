/*
 * ActivityHome
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

import com.android.vending.billing.utils.IabHelper;
import com.android.vending.billing.utils.IabResult;
import com.android.vending.billing.utils.Inventory;
import com.android.vending.billing.utils.Purchase;

import andhamil.libtamil.Konstant;
import andhamil.libtamil.Utils;
import andhamil.libtamil.view.DialogAbout;
import andhamil.libtamil.view.DialogPreferences;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

@SuppressWarnings("deprecation")
public class ActivityHome extends ActionBarActivity
{
	// Android
	private SharedPreferences mPreferences;
	private FragmentStackManager fm, sliding_menu;
	private ActionBar mActionBar;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	// In-App-Billing
	private boolean mBHasDonated = false;
	private IabHelper mIabHelper;
	private IabHelper.QueryInventoryFinishedListener mInitialInventoryListener;
	private IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mActionBar = getSupportActionBar();
		if (!android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
			mActionBar.setTitle(getString(R.string.app_name_tamil));
		else
			mActionBar.setTitle(getString(R.string.app_name_english));

		setUpView();
		menuToggeleSetUp(savedInstanceState);

		mActionBar.setDisplayShowHomeEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);

		ColorDrawable colorDrawable = new ColorDrawable();
		colorDrawable.setColor(getResources().getColor(R.color.actionbar_bg));
		mActionBar.setBackgroundDrawable(colorDrawable);

		mPreferences = getSharedPreferences(ProjectKonstants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

		mIabHelper = new IabHelper(this, ProjectKonstants.APP_PUBLIC_KEY_GOOGLE_PLAY);
		mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener()
		{
			public void onIabSetupFinished(IabResult result)
			{
				if (result.isSuccess())
				{
					mIabHelper.queryInventoryAsync(mInitialInventoryListener);
				}
			}
		});
		mInitialInventoryListener = new IabHelper.QueryInventoryFinishedListener()
		{
			public void onQueryInventoryFinished(IabResult result, Inventory inventory)
			{
				if (result.isSuccess())
				{
					mBHasDonated = inventory.hasPurchase(ProjectKonstants.APPLICATION_SKU_FOR_DONATION);
				}
				else
				{
					mBHasDonated = false;
				}
			}
		};
		mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener()
		{
			public void onIabPurchaseFinished(IabResult result, Purchase purchase)
			{
				if (result.isSuccess())
				{
					if (purchase.getSku().equals(ProjectKonstants.APPLICATION_SKU_FOR_DONATION))
					{
						Utils.showToastMsgInTamil(ActivityHome.this, getString(R.string.msg_thanks_for_donation));
						mBHasDonated = true;
						return;
					}
				}
				mBHasDonated = false;
			}
		};
	}

	void setUpView()
	{
		fm = new FragmentStackManager(this);
		sliding_menu = new FragmentStackManager(this);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		fm.addFragment(new FragmentHome(0, ActivityHome.this), R.id.main_frame, false, FragmentTransaction.TRANSIT_NONE, false);
		sliding_menu.addFragment(new FragmentSlidingMenu(getSupportFragmentManager(), ActivityHome.this), R.id.slide_fragment, false, FragmentTransaction.TRANSIT_NONE, false);
	}

	public void closeDrawer()
	{
		if (mDrawerLayout != null)
			mDrawerLayout.closeDrawers();
	}

	void menuToggeleSetUp(Bundle savedInstanceState)
	{
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.app_name_tamil)
		{
			public void onDrawerClosed(View view)
			{
				getSupportActionBar().setTitle(getString(R.string.app_name_tamil));
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView)
			{
				getSupportActionBar().setTitle(getString(R.string.app_name_tamil));
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null)
		{
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_home, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(final Menu menu)
	{
		if (android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
		{
			menu.findItem(R.id.menu_viruppangal).setTitle(getString(R.string.menu_viruppangal_english));
			menu.findItem(R.id.menu_thagaval).setTitle(getString(R.string.menu_thagaval_english));
			menu.findItem(R.id.menu_thodarbu_kol).setTitle(getString(R.string.menu_thodarbu_kol_english));
			menu.findItem(R.id.menu_parindhurai).setTitle(getString(R.string.menu_parindhurai_english));
			menu.findItem(R.id.menu_madhippeedu).setTitle(getString(R.string.menu_madhippeedu_english));

			if (mBHasDonated)
			{
				menu.findItem(R.id.menu_nankodai).setVisible(false);
			}
			else
			{
				menu.findItem(R.id.menu_nankodai).setVisible(true);
				menu.findItem(R.id.menu_nankodai).setTitle(getString(R.string.menu_nankodai_english));
			}
		}
		else
		{
			menu.findItem(R.id.menu_viruppangal).setTitle(getString(R.string.menu_viruppangal));
			menu.findItem(R.id.menu_thagaval).setTitle(getString(R.string.menu_thagaval));
			menu.findItem(R.id.menu_thodarbu_kol).setTitle(getString(R.string.menu_thodarbu_kol));
			menu.findItem(R.id.menu_parindhurai).setTitle(getString(R.string.menu_parindhurai));
			menu.findItem(R.id.menu_madhippeedu).setTitle(getString(R.string.menu_madhippeedu));

			if (mBHasDonated)
			{
				menu.findItem(R.id.menu_nankodai).setVisible(false);
			}
			else
			{
				menu.findItem(R.id.menu_nankodai).setVisible(true);
				menu.findItem(R.id.menu_nankodai).setTitle(getString(R.string.menu_nankodai));
			}
		}

		return true;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (mDrawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}
		switch (item.getItemId())
		{
		case R.id.menu_viruppangal:
			DialogPreferences dialogPreferences = new DialogPreferences(ActivityHome.this, mPreferences);
			dialogPreferences.displayDialog();
			dialogPreferences.setOnCancelListener(new OnCancelListener()
			{
				@Override
				public void onCancel(DialogInterface dialog)
				{
					Intent intentHome = new Intent(ActivityHome.this, ActivityHome.class);
					intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intentHome);
					finish();
				}
			});
			dialogPreferences.setOnDismissListener(new OnDismissListener()
			{
				@Override
				public void onDismiss(DialogInterface dialog)
				{
					Intent intentHome = new Intent(ActivityHome.this, ActivityHome.class);
					intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intentHome);
					finish();
				}
			});
			break;

		case R.id.menu_thagaval:
			DialogAbout aboutDialog = new DialogAbout(ActivityHome.this);
			aboutDialog.displayDialog(getString(R.string.about_intro), getString(R.string.about_ack));
			break;

		case R.id.menu_thodarbu_kol:
			String uriText = "mailto:" + getString(R.string.support_address_for_google_play_store);
			uriText += "?subject=" + getString(R.string.contact_email_subject_tamil);
			Uri uri = Uri.parse(uriText);
			Intent contactIntent = new Intent(Intent.ACTION_SENDTO);
			contactIntent.setData(uri);
			if (android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
			{
				startActivity(Intent.createChooser(contactIntent, getString(R.string.app_name_english)));
			}
			else
			{
				startActivity(Intent.createChooser(contactIntent, getString(R.string.app_name_tamil)));
			}
			break;

		case R.id.menu_parindhurai:
			Intent recommendIntent = new Intent(Intent.ACTION_SEND);
			recommendIntent.setType("text/plain");
			recommendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.recommend_subject_tamil));
			recommendIntent.putExtra(Intent.EXTRA_TEXT, ProjectUtils.constructRecommendMsgTamil(this));
			if (android.os.Build.VERSION.RELEASE.startsWith(Konstant.ANDROID_ICS))
			{
				startActivity(Intent.createChooser(recommendIntent, getString(R.string.app_name_english)));
			}
			else
			{
				startActivity(Intent.createChooser(recommendIntent, getString(R.string.app_name_tamil)));
			}
			break;

		case R.id.menu_madhippeedu:
			String strAppOnGooglePlayURL = getString(R.string.app_link_on_google_play_store);
			Intent intentAppOnGooglePlay = new Intent(Intent.ACTION_VIEW);
			intentAppOnGooglePlay.setData(Uri.parse(strAppOnGooglePlayURL));
			startActivity(intentAppOnGooglePlay);
			break;

		case R.id.menu_nankodai:
			AccountManager accountManager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
			if (accountManager != null)
			{
				Account[] accounts = accountManager.getAccounts();
				if (accounts.length > 0)
				{
					String strIdentifier = "[" + accounts[0].name + "][" + android.os.Build.MODEL + "][" + android.os.Build.VERSION.RELEASE + "]";
					try
					{
						mIabHelper.launchPurchaseFlow(this, ProjectKonstants.APPLICATION_SKU_FOR_DONATION, 10001, mPurchaseFinishedListener, strIdentifier);
					}
					catch (IllegalStateException e)
					{
					}
				}
				else
				{
					Utils.showToastMsgInTamil(getApplicationContext(), getResources().getString(R.string.msg_no_account_info));
				}
			}
			else
			{
				Utils.showToastMsgInTamil(getApplicationContext(), getResources().getString(R.string.msg_unexpected_error));
			}

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		try
		{
			if (mIabHelper != null)
				mIabHelper.dispose();
			mIabHelper = null;
		}
		catch (Exception e)
		{

		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
}