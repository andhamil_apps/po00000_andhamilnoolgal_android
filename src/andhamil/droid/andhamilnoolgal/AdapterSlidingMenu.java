/*
 * AdapterSlidingMenu
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

import java.util.List;

import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Utils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterSlidingMenu extends BaseAdapter
{
	List<String> mlsGroupNames;
	Context mContext;

	public AdapterSlidingMenu(Context context, List<String> data)
	{
		this.mContext = context;
		this.mlsGroupNames = data;
	}

	@Override
	public int getCount()
	{
		return mlsGroupNames.size();
	}

	@Override
	public Object getItem(int id)
	{
		return id;
	}

	@Override
	public long getItemId(int id)
	{
		return id;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
			row = layoutInflater.inflate(R.layout.listitem_slidingmenu, parent, false);
		}

		TextView tvGroupName = (TextView) row.findViewById(R.id.tv_li_navdrawer);
		tvGroupName.setTypeface(Utils.FONT_TAMIL_NORMAL);

		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
			tvGroupName.setText(EncoderFontBamini.encode(mlsGroupNames.get(position)));
		else
			tvGroupName.setText(mlsGroupNames.get(position));

		row.setTag(tvGroupName);

		return row;
	}

	public class ViewHolder
	{
		TextView name;
	}

}
