/*
 * ParserJSON
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ParserJSON
{
	@SuppressWarnings("rawtypes")
	public static void decode(String strJSON)
	{
		ProjectKonstants.BOOK_DATA.wipeoutData();

		try
		{
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(strJSON);

			ArrayList<Group> algGroups = new ArrayList<Group>();

			JSONArray jarrGroups = (JSONArray) jsonObject.get("groups");
			Iterator itrGroups = jarrGroups.iterator();
			while (itrGroups.hasNext())
			{
				JSONObject jobjGroup = (JSONObject) itrGroups.next();
				String strGroupName = jobjGroup.get("group_name").toString();

				Group group = new Group();

				ArrayList<Book> albBooks = new ArrayList<Book>();

				JSONArray jarrBooks = (JSONArray) jobjGroup.get("books");
				Iterator itrBooks = jarrBooks.iterator();
				while (itrBooks.hasNext())
				{
					JSONObject jobjBook = (JSONObject) itrBooks.next();

					Book book = new Book();
					book.setBookName(jobjBook.get("book_name").toString());
					book.setBookAuthor(jobjBook.get("nool_iyatriyavar").toString());
					book.setExplanationAuthor(jobjBook.get("uraiyaasiriyar").toString());
					book.setAppName(jobjBook.get("app_name").toString());
					book.setPackageName(jobjBook.get("package_name").toString());
					book.setAppStoreLink(jobjBook.get("google_playstore_link").toString());

					albBooks.add(book);
				}

				group.setGroupName(strGroupName);
				group.setBooks(albBooks);

				algGroups.add(group);
			}

			ProjectKonstants.BOOK_DATA.setGroups(algGroups);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}
}
