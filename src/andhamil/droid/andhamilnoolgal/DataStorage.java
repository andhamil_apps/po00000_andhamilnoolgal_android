/*
 * DataStorage
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.droid.andhamilnoolgal;

import java.util.ArrayList;

class Book
{
	private String mstrBookName = "";
	private String mstrBookAuthor = "";
	private String mstrExplanationAuthor = "";
	private String mstrAppName = "";
	private String mstrPackageName = "";
	private String mstrAppStoreLink = "";

	public String getBookName()
	{
		return mstrBookName;
	}

	public void setBookName(String strBookName)
	{
		this.mstrBookName = strBookName;
	}

	public String getBookAuthor()
	{
		return mstrBookAuthor;
	}

	public void setBookAuthor(String strBookAuthor)
	{
		this.mstrBookAuthor = strBookAuthor;
	}

	public String getExplanationAuthor()
	{
		return mstrExplanationAuthor;
	}

	public void setExplanationAuthor(String strExplanationAuthor)
	{
		this.mstrExplanationAuthor = strExplanationAuthor;
	}

	public String getAppName()
	{
		return mstrAppName;
	}

	public void setAppName(String strAppName)
	{
		this.mstrAppName = strAppName;
	}

	public String getPackageName()
	{
		return mstrPackageName;
	}

	public void setPackageName(String strPackageName)
	{
		this.mstrPackageName = strPackageName;
	}

	public String getAppStoreLink()
	{
		return mstrAppStoreLink;
	}

	public void setAppStoreLink(String strAppStoreLink)
	{
		this.mstrAppStoreLink = strAppStoreLink;
	}
}

class Group
{
	private String mstrGroupName = "";
	private ArrayList<Book> malbBooks = new ArrayList<Book>();

	public String getGroupName()
	{
		return mstrGroupName;
	}

	public void setGroupName(String strGroupName)
	{
		this.mstrGroupName = strGroupName;
	}

	public ArrayList<Book> getBooks()
	{
		return malbBooks;
	}

	public void setBooks(ArrayList<Book> albBooks)
	{
		this.malbBooks = albBooks;
	}
}

public class DataStorage
{
	private ArrayList<Group> malgGroups =  new ArrayList<Group>();

	public ArrayList<Group> getGroups()
	{
		return malgGroups;
	}

	public void setGroups(ArrayList<Group> algGroups)
	{
		this.malgGroups = algGroups;
	}

	public void wipeoutData()
	{
		for (int i = 0; i < malgGroups.size(); i++)
		{
			Group group = malgGroups.get(i);
			group.setGroupName("");
			for (int j = 0; j < group.getBooks().size(); j++)
			{
				Book book = group.getBooks().get(j);
				book.setBookName("");
				book.setBookAuthor("");
				book.setExplanationAuthor("");
				book.setAppName("");
				book.setPackageName("");
				book.setAppStoreLink("");
			}
		}
	}
}
