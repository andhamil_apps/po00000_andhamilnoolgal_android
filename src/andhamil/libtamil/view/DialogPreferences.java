/*
 * DialogPreferences
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.droid.andhamilnoolgal.R;
import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Konstant;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class DialogPreferences extends Dialog
{
	// Primitives
	private int mnBrightness;

	// Android
	private Context mContext;
	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	// UI Controls
	private TextView mtvTitle;
	private TextView mtvBrightness;
	private SeekBar msbBrightness;
	private TextView mtvFontSize;
	private SeekBar msbFontSize;
	private TextView mtvFont;
	private RadioGroup mrgFont;
	private RadioButton mrbBarathi;
	private RadioButton mrbKabilar;
	private RadioButton mrbKambar;
	private RadioButton mrbKaveri;
	private RadioButton mrbValluvar;

	public DialogPreferences(Context context, SharedPreferences preferences)
	{
		super(context);
		mContext = context;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_preferences);

		mPreferences = preferences;
		mPrefEditor = preferences.edit();
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		dismiss();
	}

	public void displayDialog()
	{
		intializeViews();
		setFont();
		setFontSize();
		setViewData();
		addActions();

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_pref_title);
		mtvBrightness = (TextView) findViewById(R.id.tv_pref_brightness);
		msbBrightness = (SeekBar) findViewById(R.id.sb_pref_brightness);
		mtvFontSize = (TextView) findViewById(R.id.tv_pref_fontsize);
		msbFontSize = (SeekBar) findViewById(R.id.sb_pref_fontsize);
		mtvFont = (TextView) findViewById(R.id.tv_pref_font);
		mrgFont = (RadioGroup) findViewById(R.id.rg_pref_font);
		mrbBarathi = (RadioButton) findViewById(R.id.rb_pref_barathi);
		mrbKabilar = (RadioButton) findViewById(R.id.rb_pref_kabilar);
		mrbKambar = (RadioButton) findViewById(R.id.rb_pref_kambar);
		mrbKaveri = (RadioButton) findViewById(R.id.rb_pref_kaveri);
		mrbValluvar = (RadioButton) findViewById(R.id.rb_pref_valluvar);

		return;
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvBrightness.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvFontSize.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvFont.setTypeface(Utils.FONT_TAMIL_BOLD);
		mrbBarathi.setTypeface(Utils.FONT_TAMIL_BAMINI_NORMAL);
		mrbKabilar.setTypeface(Utils.FONT_TAMIL_BARATHI_NORMAL);
		mrbKambar.setTypeface(Utils.FONT_TAMIL_KAMBAR_NORMAL);
		mrbKaveri.setTypeface(Utils.FONT_TAMIL_KAVERI_NORMAL);
		mrbValluvar.setTypeface(Utils.FONT_TAMIL_VALLUVAR_NORMAL);

		return;
	}

	private void setFontSize()
	{
		mtvTitle.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvBrightness.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvFontSize.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mtvFont.setTextSize(mContext.getResources().getInteger(R.integer.font_l));
		mrbBarathi.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mrbKabilar.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mrbKambar.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mrbKaveri.setTextSize(mContext.getResources().getInteger(R.integer.font_m));
		mrbValluvar.setTextSize(mContext.getResources().getInteger(R.integer.font_m));

		return;
	}

	private void setViewData()
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.preferences)));

			mtvBrightness.setText(EncoderFontBamini.encode(mContext.getString(R.string.brightness)));
			msbBrightness.setProgress(mPreferences.getInt(Konstant.PREF_APP_BRIGHTNESS, Konstant.DEF_APP_BRIGHTNESS));

			mtvFontSize.setText(EncoderFontBamini.encode(mContext.getString(R.string.fontsize)));
			msbFontSize.setProgress(mPreferences.getInt(Konstant.PREF_APP_FONTSIZE, Konstant.DEF_APP_FONTSIZE));

			mtvFont.setText(EncoderFontBamini.encode(mContext.getString(R.string.font)));
		}
		else
		{
			mtvTitle.setText(mContext.getString(R.string.preferences));

			mtvBrightness.setText(mContext.getString(R.string.brightness));
			msbBrightness.setProgress(mPreferences.getInt(Konstant.PREF_APP_BRIGHTNESS, Konstant.DEF_APP_BRIGHTNESS));

			mtvFontSize.setText(mContext.getString(R.string.fontsize));
			msbFontSize.setProgress(mPreferences.getInt(Konstant.PREF_APP_FONTSIZE, Konstant.DEF_APP_FONTSIZE));

			mtvFont.setText(mContext.getString(R.string.font));
		}

		mrbBarathi.setText(EncoderFontBamini.encode(mContext.getString(R.string.font_bamini)));
		mrbKabilar.setText(mContext.getString(R.string.font_barathi));
		mrbKambar.setText(mContext.getString(R.string.font_kambar));
		mrbKaveri.setText(mContext.getString(R.string.font_kaveri));
		mrbValluvar.setText(mContext.getString(R.string.font_valluvar));

		int nFont = mPreferences.getInt(Konstant.PREF_APP_FONT, Konstant.DEF_APP_FONT);
		switch (nFont)
		{
		case Konstant.ID_FONT_BAMINI:
			mrbBarathi.setChecked(true);
			break;
		case Konstant.ID_FONT_BARATHI:
			mrbKabilar.setChecked(true);
			break;
		case Konstant.ID_FONT_KAMBAR:
			mrbKambar.setChecked(true);
			break;
		case Konstant.ID_FONT_KAVERI:
			mrbKaveri.setChecked(true);
			break;
		case Konstant.ID_FONT_VALLUVAR:
			mrbValluvar.setChecked(true);
			break;
		}
		return;
	}

	private void addActions()
	{
		msbBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
		{
			@Override
			public void onStopTrackingTouch(SeekBar seekBar)
			{

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar)
			{
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
			{
				if (progress <= 20)
				{
					mnBrightness = 20;
				}
				else
				{
					mnBrightness = progress;
				}

				LayoutParams layoutpars = getWindow().getAttributes();
				layoutpars.screenBrightness = mnBrightness / (float) 255;
				getWindow().setAttributes(layoutpars);

				mPrefEditor.putInt(Konstant.PREF_APP_BRIGHTNESS, mnBrightness);
				mPrefEditor.commit();
			}
		});

		msbFontSize.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
		{
			@Override
			public void onStopTrackingTouch(SeekBar seekBar)
			{

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar)
			{
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
			{
				mPrefEditor.putInt(Konstant.PREF_APP_FONTSIZE, progress);
				mPrefEditor.commit();
			}
		});

		mrgFont.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				int nFont = 0;
				if (group.getCheckedRadioButtonId() == R.id.rb_pref_barathi)
					nFont = Konstant.ID_FONT_BAMINI;
				else if (group.getCheckedRadioButtonId() == R.id.rb_pref_kabilar)
					nFont = Konstant.ID_FONT_BARATHI;
				else if (group.getCheckedRadioButtonId() == R.id.rb_pref_kambar)
					nFont = Konstant.ID_FONT_KAMBAR;
				else if (group.getCheckedRadioButtonId() == R.id.rb_pref_kaveri)
					nFont = Konstant.ID_FONT_KAVERI;
				else if (group.getCheckedRadioButtonId() == R.id.rb_pref_valluvar)
					nFont = Konstant.ID_FONT_VALLUVAR;

				mPrefEditor.putInt(Konstant.PREF_APP_FONT, nFont);
				mPrefEditor.commit();

				switch (nFont)
				{
				case Konstant.ID_FONT_BAMINI:
					Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_BAMINI_BOLD;
					Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_BAMINI_NORMAL;
					break;
				case Konstant.ID_FONT_BARATHI:
					Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_BARATHI_BOLD;
					Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_BARATHI_NORMAL;
					break;
				case Konstant.ID_FONT_KAMBAR:
					Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_KAMBAR_BOLD;
					Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_KAMBAR_NORMAL;
					break;
				case Konstant.ID_FONT_KAVERI:
					Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_KAVERI_BOLD;
					Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_KAVERI_NORMAL;
					break;
				case Konstant.ID_FONT_VALLUVAR:
					Utils.FONT_TAMIL_BOLD = Utils.FONT_TAMIL_VALLUVAR_BOLD;
					Utils.FONT_TAMIL_NORMAL = Utils.FONT_TAMIL_VALLUVAR_NORMAL;
					break;
				}
			}
		});

		return;
	}
}
