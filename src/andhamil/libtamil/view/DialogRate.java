/*
 * DialogRate
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2015.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.droid.andhamilnoolgal.R;
import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Konstant;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class DialogRate extends Dialog
{
	// Primitives
	private int mnUserSelection;

	// Android
	private Context mContext;
	private SharedPreferences mPreferences;

	// UI Controls
	private TextView tvTitle;
	private TextView tvRatePrompt;
	private TextView tvYeah;
	private TextView tvNope;
	private TextView tvLater;

	public DialogRate(Context context, SharedPreferences preferences)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_rate);
		this.setCancelable(false);

		mContext = context;
		mPreferences = preferences;

		Utils.loadFontFiles(mContext, mPreferences);
	}

	public void displayDialog()
	{
		intializeViews();
		setFont();
		setViewData();
		addActions();

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}

	public int getUserSelection()
	{
		return mnUserSelection;
	}

	private void intializeViews()
	{
		tvTitle = (TextView) findViewById(R.id.tv_rate_title);
		tvRatePrompt = (TextView) findViewById(R.id.tv_rate_text);
		tvYeah = (TextView) findViewById(R.id.tv_rate_yeah);
		tvNope = (TextView) findViewById(R.id.tv_rate_nope);
		tvLater = (TextView) findViewById(R.id.tv_rate_later);

		return;
	}

	private void setFont()
	{
		tvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		tvRatePrompt.setTypeface(Utils.FONT_TAMIL_NORMAL);
		tvYeah.setTypeface(Utils.FONT_TAMIL_NORMAL);
		tvNope.setTypeface(Utils.FONT_TAMIL_NORMAL);
		tvLater.setTypeface(Utils.FONT_TAMIL_NORMAL);

		return;
	}

	private void setViewData()
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			tvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.rate_this_app)));
			tvRatePrompt.setText(EncoderFontBamini.encode(mContext.getString(R.string.rate_this_app_prompt)));
			tvYeah.setText(EncoderFontBamini.encode(mContext.getString(R.string.yeah)));
			tvNope.setText(EncoderFontBamini.encode(mContext.getString(R.string.nope)));
			tvLater.setText(EncoderFontBamini.encode(mContext.getString(R.string.later)));
		}
		else
		{
			tvTitle.setText(mContext.getString(R.string.rate_this_app));
			tvRatePrompt.setText(mContext.getString(R.string.rate_this_app_prompt));
			tvYeah.setText(mContext.getString(R.string.yeah));
			tvNope.setText(mContext.getString(R.string.nope));
			tvLater.setText(mContext.getString(R.string.later));
		}

		return;
	}

	private void addActions()
	{
		final SharedPreferences.Editor prefEditor = mPreferences.edit();

		tvYeah.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mnUserSelection = Konstant.ID_RATE_YEAH;

				prefEditor.putInt(Konstant.PREF_RATE_THIS_APP, Konstant.ID_RATE_YEAH);
				prefEditor.commit();

				dismiss();
			}
		});

		tvNope.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mnUserSelection = Konstant.ID_RATE_NOPE;

				prefEditor.putInt(Konstant.PREF_RATE_THIS_APP, Konstant.ID_RATE_NOPE);
				prefEditor.commit();

				dismiss();
			}
		});

		tvLater.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mnUserSelection = Konstant.ID_RATE_LATER;

				prefEditor.putInt(Konstant.PREF_RATE_THIS_APP, Konstant.ID_RATE_LATER);
				prefEditor.commit();

				dismiss();
			}
		});
		
		return;
	}
}