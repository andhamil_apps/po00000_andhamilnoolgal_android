/*
 * DialogAbout
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil.view;

import andhamil.droid.andhamilnoolgal.R;
import andhamil.libtamil.EncoderFontBamini;
import andhamil.libtamil.Utils;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

public class DialogAbout extends Dialog
{
	// Android
	private Context mContext;

	// UI Controls
	private TextView mtvTitle;
	private TextView mtvIntro;
	private TextView mtvAck;
	private TextView mtvSocialConnection;
	private ImageButton mibFacebook;
	private ImageButton mibTwitters;
	private ImageButton mibGooglePlus;

	public DialogAbout(Context context)
	{
		super(context);
		mContext = context;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_about);
	}

	public void displayDialog(String strIntro, String strAck)
	{

		intializeViews();
		validateViews(strIntro, strAck);
		setFont();
		setViewData(strIntro, strAck);
		addActions();

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}

	private void intializeViews()
	{
		mtvTitle = (TextView) findViewById(R.id.tv_about_title);
		mtvIntro = (TextView) findViewById(R.id.tv_about_intro);
		mtvAck = (TextView) findViewById(R.id.tv_about_acknowledgement);
		mtvSocialConnection = (TextView) findViewById(R.id.tv_about_social_connection);
		mibFacebook = (ImageButton) findViewById(R.id.ib_about_facebook);
		mibTwitters = (ImageButton) findViewById(R.id.ib_about_twitter);
		mibGooglePlus = (ImageButton) findViewById(R.id.ib_about_googleplus);

		return;
	}

	private void validateViews(String strIntro, String strAck)
	{
		if (strIntro.isEmpty())
		{
			mtvIntro.setVisibility(View.GONE);
		}
		else
		{
			mtvIntro.setVisibility(View.VISIBLE);
		}

		if (strAck.isEmpty())
		{
			mtvAck.setVisibility(View.GONE);
		}
		else
		{
			mtvAck.setVisibility(View.VISIBLE);
		}
	}

	private void setFont()
	{
		mtvTitle.setTypeface(Utils.FONT_TAMIL_BOLD);
		mtvIntro.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvAck.setTypeface(Utils.FONT_TAMIL_NORMAL);
		mtvSocialConnection.setTypeface(Utils.FONT_TAMIL_NORMAL);
		return;
	}

	private void setViewData(String strIntro, String strAck)
	{
		if (Utils.FONT_TAMIL_NORMAL == Utils.FONT_TAMIL_BAMINI_NORMAL)
		{
			mtvTitle.setText(EncoderFontBamini.encode(mContext.getString(R.string.about)));
			mtvIntro.setText(EncoderFontBamini.encode(strIntro));
			mtvAck.setText(EncoderFontBamini.encode(strAck));
			mtvSocialConnection.setText(EncoderFontBamini.encode(mContext.getString(R.string.social_network_prompt)));
		}
		else
		{
			mtvTitle.setText(mContext.getString(R.string.about));
			mtvIntro.setText(strIntro);
			mtvAck.setText(strAck);
			mtvSocialConnection.setText(mContext.getString(R.string.social_network_prompt));
		}
		return;
	}

	private void addActions()
	{
		mibFacebook.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnFbURL = mContext.getString(R.string.page_link_on_facebook);
				Intent intentPageOnFb = new Intent(Intent.ACTION_VIEW);
				intentPageOnFb.setData(Uri.parse(strPageOnFbURL));
				mContext.startActivity(intentPageOnFb);
			}
		});

		mibTwitters.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnTwitterURL = mContext.getString(R.string.page_link_on_twitter);
				Intent intentPageOnTwitter = new Intent(Intent.ACTION_VIEW);
				intentPageOnTwitter.setData(Uri.parse(strPageOnTwitterURL));
				mContext.startActivity(intentPageOnTwitter);
			}
		});

		mibGooglePlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strPageOnGpURL = mContext.getString(R.string.page_link_on_googleplus);
				Intent intentPageOnGp = new Intent(Intent.ACTION_VIEW);
				intentPageOnGp.setData(Uri.parse(strPageOnGpURL));
				mContext.startActivity(intentPageOnGp);
			}
		});

		return;
	}
}