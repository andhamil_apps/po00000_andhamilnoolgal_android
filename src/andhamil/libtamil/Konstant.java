/*
 * Konstant
 * 
 * @ author: Karthik Palanivelu [karthik@andhamil.com]
 * Andhamil, 2013-2038.
 * Confidential and proprietary.
 */

package andhamil.libtamil;

import java.util.ArrayList;

public interface Konstant
{
	// Filenames

	public static String FILENAME_FONT_TAMIL_BAMINI_BOLD = "fonts/tamil_bamini.ttf";
	public static String FILENAME_FONT_TAMIL_BAMINI_NORMAL = "fonts/tamil_bamini.ttf";
	public static String FILENAME_FONT_TAMIL_BARATHI_BOLD = "fonts/tamil_barathi_bold.ttf";
	public static String FILENAME_FONT_TAMIL_BARATHI_NORMAL = "fonts/tamil_barathi_normal.ttf";
	public static String FILENAME_FONT_TAMIL_KAMBAR_BOLD = "fonts/tamil_kambar_bold.ttf";
	public static String FILENAME_FONT_TAMIL_KAMBAR_NORMAL = "fonts/tamil_kambar_normal.ttf";
	public static String FILENAME_FONT_TAMIL_KAVERI_BOLD = "fonts/tamil_kaveri_bold.ttf";
	public static String FILENAME_FONT_TAMIL_KAVERI_NORMAL = "fonts/tamil_kaveri_normal.ttf";
	public static String FILENAME_FONT_TAMIL_VALLUVAR_BOLD = "fonts/tamil_valluvar_bold.ttf";
	public static String FILENAME_FONT_TAMIL_VALLUVAR_NORMAL = "fonts/tamil_valluvar_normal.ttf";

	// Application Versions

	public static final String ANDROID_ICS = "4.0";
	public static final String ANDROID_JB_1 = "4.1";
	public static final String ANDROID_JB_2 = "4.2";
	public static final String ANDROID_KK_1 = "4.3";
	public static final String ANDROID_KK_2 = "4.4";
	public static final String ANDROID_LP = "5.0";

	// Application

	public static int ID_RATE_YEAH = 0;
	public static int ID_RATE_NOPE = 1;
	public static int ID_RATE_LATER = 2;

	public static int ID_SHARE_IMAGE = 0;
	public static int ID_SHARE_TEXT = 1;

	public static int ID_FONT_BAMINI = 0;
	public static int ID_FONT_BARATHI = 1;
	public static int ID_FONT_KAMBAR = 2;
	public static int ID_FONT_KAVERI = 3;
	public static int ID_FONT_VALLUVAR = 4;

	// Shared Preferences

	public static final String PREF_RATE_THIS_APP = "RATE_THIS_APP";
	public static int DEF_RATE_THIS_APP = ID_RATE_LATER;

	public static final String PREF_LAUNCH_COUNT = "LAUNCH_COUNT_FOR_RATING";
	public static int DEF_LAUNCH_COUNT = 0;

	public static final String PREF_CURRENT_CHEIYUL_NO = "CURRENT_CHEIYUL_NO";
	public static final int DEF_CURRENT_CHEIYUL_NO = 0;

	public static final String PREF_WIDGET_CHEIYUL_NO = "WIDGET_CHEIYUL_NO";
	public static final int DEF_WIDGET_CHEIYUL_NO = 0;

	public static final String PREF_CURRENT_SCROLL_YPOS = "CURRENT_SCROLL_YPOS";
	public static final int DEF_CURRENT_SCROLL_YPOS = 0;

	public static final String PREF_APP_BRIGHTNESS = "APP_BRIGHTNESS";
	public static int DEF_APP_BRIGHTNESS = 0;
	
	public static final String PREF_APP_FONTSIZE = "APP_FONTSIZE";
	public static int DEF_APP_FONTSIZE = 0;
	
	public static final String PREF_APP_FONT = "APP_FONT";
	public static int DEF_APP_FONT = ID_FONT_KAMBAR;

	// Exchange Keys

	public static String KEY_CHEIYUL_NO = "KEY_CHEIYUL_NO";

	public static final String ARG_PAGE = "page";

	// Data

	public ArrayList<String> INSTALLED_APPS = new ArrayList<String>();
	public String ANDHAMIL_BOOKS_PACKAGENAME = "andhamil.droid.andhamilnoolgal";
}